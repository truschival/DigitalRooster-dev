# DigitalRooster.dev

Source repository for website [www.digitalRooster.dev](www.digitalrooster.dev) 

Check out other parts of the DigitalRooster project:

*  [DigitalRoosterGui](https://github.com/truschival/DigitalRoosterGui) : QML-Application
*  [DigitalRoosterREST](https://github.com/truschival/DigitalRooster_REST) : a quickhack OpenAPI webservice to
   configure DigitalRoosterGui (this needs professional attention!) 
*  [DigitalRooster.dev](https://github.com/truschival/DigitalRooster.dev) : Source of this website
*  [Buildroot external](https://github.com/truschival/buildroot_digitalrooster) : to build the embedded linux for your hardware.
